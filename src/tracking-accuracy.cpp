#include "tracking-accuracy.h"

#define S 30

void TrackingAccuracy::addBbox(const cv::Rect2d& bbox, const cv::Rect2d& gt)
{
    if (std::isnan(gt.x))
        return;

    m_samples++;

    if (std::isnan(bbox.x))
    {
        m_failures++;
        m_jaccardLastCalculated = 0.;
    }
    else
    {
        double areaBbox = bbox.width * bbox.height;
        double areaGt = gt.width * gt.height;
        int32_t intersectionX = std::min(bbox.x + bbox.width, gt.x + gt.width) - std::max(bbox.x, gt.x);
        int32_t intersectionY = std::min(bbox.y + bbox.height, gt.y + gt.height) - std::max(bbox.y, gt.y);

        double intersection = intersectionX > 0 && intersectionY > 0
            ? intersectionX * intersectionY
            : 0.;

        m_jaccardLastCalculated = intersection / (areaBbox + areaGt - intersection);

        if (m_jaccardLastCalculated <= 0.)
            m_failures++;

        m_jaccardSum += m_jaccardLastCalculated;
    }

    m_jaccardAverage = m_jaccardSum / static_cast<double>(m_samples);
    double failureFactor = static_cast<double>(m_failures) / static_cast<double>(m_samples);
    m_robustness = std::exp(-S * failureFactor);
}
