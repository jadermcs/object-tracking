#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <regex>
#include "tracking-accuracy.h"

// Convert to string
#define SSTR(x) static_cast<std::ostringstream&>( \
    (std::ostringstream() << x)).str()

#ifndef CONTRIB_VERSION
#define CONTRIB_VERSION CV_MAJOR_VERSION
#endif

#if CONTRIB_VERSION == 3
#define PORTABLE_CREATE_TRACKER(tracker) cv::tracker::createTracker();
#elif CONTRIB_VERSION == 4
#define PORTABLE_CREATE_TRACKER(tracker) cv::tracker::create();
#else
#error("Unsupported OpenCV version. Supported: 3 or 4")
#endif // CONTRIB_VERSION

#define UNSUPPORTED_TRACKER(name) \
    std::cout << "This tracker (" << name << ") is not supported because this software" \
    " was compiled using OpenCV " << CV_MAJOR_VERSION << ", but 4 is required\n"; \
    std::exit(0);

#define WINDOW_TITLE "Tracking"

cv::String doubleToString(double n);
cv::Rect2d safeRect(const cv::Rect2d& r);
void createTracker(int32_t type, cv::Ptr<cv::Tracker>& tracker, cv::String& trackerName);

int main(int argc, char **argv) {
    setvbuf(stdout, nullptr, _IONBF, 0);
    std::vector<std::string> args(argv + 1, argv + argc);

    cv::CommandLineParser commandLineParser(argc, argv,
       "{help h usage ? | | shows this message}"
       "{@folder | data/car1/ | frames for tracking folder}"
       "{@filegt | data/gtcar1.txt | ground true for bouding boxes}"
       "{tracker | 2 | algorithm to use for object tracking "
       "\n\t\t 0:BOOSTING"
       "\n\t\t 1:MIL"
       "\n\t\t 2:KCF"
       "\n\t\t 3:TLD"
       "\n\t\t 4:MEDIANFLOW"
       "\n\t\t 5:GOTURN"
       "\n\t\t 6:MOSSE"
       "\n\t\t 7:CSRT}"
    );

    if (!commandLineParser.check()) {
        commandLineParser.printErrors();
        return 0;
    }

    if (commandLineParser.has("help")) {
        commandLineParser.about(
            "Object Tracking\n\n"
            "Predicts the object position in frame.\n"
        );
        commandLineParser.printMessage();
        return 0;
    }

    cv::String folder = commandLineParser.get<cv::String>("@folder");
    cv::String filegt = commandLineParser.get<cv::String>("@filegt");
    int32_t trackerType = commandLineParser.get<int32_t>("tracker");
    cv::String trackerName;
    cv::Ptr<cv::Tracker> tracker;

    createTracker(trackerType, tracker, trackerName);

    if (folder.size() > 0 && folder[folder.size() - 1] != '/') {
        folder += '/';
    }
    folder += "*.jpg";

    std::vector<cv::String> files;
    cv::glob(folder, files, false);

    std::ifstream filegtStream(filegt);
    std::string fileData;

    filegtStream.seekg(0, std::ios::end);
    fileData.reserve(filegtStream.tellg());
    filegtStream.seekg(0, std::ios::beg);
    fileData.assign((std::istreambuf_iterator<char>(filegtStream)), std::istreambuf_iterator<char>());

    std::regex rectMatcher("([0-9.-]+|NaN),([0-9.-]+|NaN),([0-9.-]+|NaN),([0-9.-]+|NaN)");
    std::vector<cv::Rect2d> groundTruthBboxes;
    std::smatch matches;

    while (std::regex_search(fileData, matches, rectMatcher)) {
        cv::Rect2d rect;
        if (matches[1].str() == "NaN") {
            rect.x = rect.y = rect.width = rect.height = NAN;
        } else {
            rect.x = std::atof(matches[1].str().c_str());
            rect.y = std::atof(matches[2].str().c_str());
            rect.width = std::atof(matches[3].str().c_str()) - rect.x;
            rect.height = std::atof(matches[4].str().c_str()) - rect.y;
        }
//        std::cout << "Matched: " << matches[0].str() << "\n";
//        std::cout << "Box: x = " << rect.x << ", y = " << rect.y << ", w = " << rect.width << ", h = " << rect.height << "\n";
        groundTruthBboxes.push_back(rect);
        fileData = matches.suffix().str();
    }

    if (groundTruthBboxes.empty())
    {
        std::cout << "Failed to read ground truth bouding boxes.\n";
        return 1;
    }

    cv::Mat frame;

    if (!files.empty())
        frame = cv::imread(files[0].c_str(), cv::IMREAD_COLOR);

    if (frame.empty())
    {
        std::cout << "Failed to read first frame.\n";
        return 1;
    }

    cv::rectangle(frame, groundTruthBboxes[0], cv::Scalar( 255, 0, 0 ), 2, 1);
    cv::namedWindow(WINDOW_TITLE, cv::WINDOW_AUTOSIZE);
    cv::imshow(WINDOW_TITLE, frame);

    if (!tracker->init(frame, safeRect(groundTruthBboxes[0])))
        std::cout << "Failed to intialize tracker.\n";

    TrackingAccuracy trackingAccuracy;
    cv::Rect2d bboxNaN(NAN, NAN, NAN, NAN);
    bool restartTracker = false;

    double averageFps = 0.;

    uint32_t currentFrame = 0;

    for (currentFrame = 0; currentFrame < files.size(); currentFrame++) {
        frame = cv::imread(files[currentFrame].c_str());
        // Start timer
        double timer = (double)cv::getTickCount();

        cv::Rect2d groundTruthBbox = currentFrame < groundTruthBboxes.size()
            ? groundTruthBboxes[currentFrame]
            : bboxNaN;

        if (restartTracker && !std::isnan(groundTruthBbox.x))
        {
            tracker.release();
            createTracker(trackerType, tracker, trackerName);

            if (tracker->init(frame, safeRect(groundTruthBbox)))
                restartTracker = false;
            else
                std::cout << "Failed to restart tracker at frame " << currentFrame << "\n";
        }

        // Update the tracking result
        cv::Rect2d trackedBbox;
        bool ok = tracker->update(frame, trackedBbox);

        // Calculate Frames per second (FPS)
        float fps = cv::getTickFrequency() / ((double)cv::getTickCount() - timer);

        averageFps += fps;

        trackingAccuracy.addBbox(
            ok ? trackedBbox : bboxNaN,
            groundTruthBbox
        );

        bool niceGuess = trackingAccuracy.getLastJaccard() > 0.;

//        std::cout << "Frame: " << currentFrame << ", tracked: " << ok << ", nice guess?: " << niceGuess << "\n";

        if (!niceGuess)
            restartTracker = true;

        if (ok)
        {
            // Tracking success : Draw the tracked object
            cv::rectangle(frame, trackedBbox, cv::Scalar(255, 0, 0), 1);
            cv::putText(frame, "Result", cv::Point(trackedBbox.x + trackedBbox.width - 52, trackedBbox.y + 13),
                        cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0), 1);
        }
        else
        {
            cv::putText(frame, "Tracking failed", cv::Point(8, 96),
                        cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
        }

        cv::String labelAccuracy = "Jacc. Avg.: " +
            doubleToString(trackingAccuracy.getJaccardAverage()) +
            "  Robustness: " +
            doubleToString(trackingAccuracy.getRobustness());

        cv::putText(frame, labelAccuracy, cv::Point(8, 40),
                    cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 128, 75), 2);

        if (currentFrame >= groundTruthBboxes.size()) {
            cv::putText(frame, "No ground truth", cv::Point(8, 60),
                        cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
        } else if (std::isnan(groundTruthBbox.x)) {
            cv::putText(frame, "Untrackable", cv::Point(6, 68),
                        cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
        } else {
            cv::rectangle(frame, groundTruthBbox, cv::Scalar(255, 0, 255), 1, 1);
            cv::putText(frame, "GT", cv::Point(groundTruthBbox.x, groundTruthBbox.y - 2),
                        cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 255), 1);
        }

        // Display tracker type on frame
        constexpr auto trackerLabelScale = 0.5;
        constexpr auto trackerLabelThickness = 2;
        cv::Point trackerLabelPosition(8, 20);
        cv::String trackerLabel = "Tracker: " + trackerName;
        cv::Size trackerLabelSize = cv::getTextSize(
            trackerLabel, cv::FONT_HERSHEY_SIMPLEX,
            trackerLabelScale, trackerLabelThickness, nullptr
        );
        cv::putText(frame, trackerLabel, trackerLabelPosition, cv::FONT_HERSHEY_SIMPLEX,
                    trackerLabelScale, cv::Scalar(50, 170, 50), trackerLabelThickness);

        cv::String labelFps = " / FPS : " + SSTR(int(fps));
        cv::Point labelFpsPosition = trackerLabelPosition + cv::Point(trackerLabelSize.width, 0);
        cv::putText(frame, labelFps, labelFpsPosition, cv::FONT_HERSHEY_SIMPLEX,
                    trackerLabelScale, cv::Scalar(50, 170, 50), trackerLabelThickness);

        // Display frame.
        cv::imshow(WINDOW_TITLE, frame);

        // Exit if ESC pressed.
        int32_t k = cv::waitKeyEx(1);

        if (k == 27 || k == 'q')
            break;
    }

    if (currentFrame > 0)
        averageFps /= currentFrame;

    printf("Average FPS: %.2f, average jaccard: %.2f, robustness: %.2f\n",
           averageFps, trackingAccuracy.getJaccardAverage(), trackingAccuracy.getRobustness());
    std::cout << "Press any key to quit...\n";

    cv::waitKeyEx();
    cv::destroyAllWindows();
    return 0;
}

cv::String doubleToString(double n)
{
    char buffer[128];
    snprintf(buffer, sizeof(buffer), "%.2f", n);
    return cv::String(buffer);
}

cv::Rect2d safeRect(const cv::Rect2d& r)
{
    cv::Rect2d result(r);
    result.x = std::max(0., std::floor(result.x));
    result.y = std::max(0., std::floor(result.y));
    result.width = std::floor(result.width);
    result.height = std::floor(result.height);
    return result;
}

void createTracker(int32_t type, cv::Ptr<cv::Tracker>& tracker, cv::String& trackerName)
{
    switch (type) {
        case 0:
            trackerName = "Boosting";
            tracker = PORTABLE_CREATE_TRACKER(TrackerBoosting)
            break;
        case 1:
            trackerName = "MIL";
            tracker = PORTABLE_CREATE_TRACKER(TrackerMIL)
            break;
        case 2:
            trackerName = "KCF";
            tracker = PORTABLE_CREATE_TRACKER(TrackerKCF)
            break;
        case 3:
            trackerName = "TLD";
            tracker = PORTABLE_CREATE_TRACKER(TrackerTLD)
            break;
        case 4:
            trackerName = "MedianFlow";
            tracker = PORTABLE_CREATE_TRACKER(TrackerMedianFlow)
            break;
        case 5:
            trackerName = "GOTURN";
            tracker = PORTABLE_CREATE_TRACKER(TrackerGOTURN)
            break;
        case 6:
#if CV_MAJOR_VERSION == 4
            trackerName = "MOSSE";
            tracker = PORTABLE_CREATE_TRACKER(TrackerMOSSE)
            break;
#else
            UNSUPPORTED_TRACKER("MOSSE")
#endif // CV_MAJOR_VERSION
        case 7:
#if CV_MAJOR_VERSION == 4
            trackerName = "CSRT";
            tracker = PORTABLE_CREATE_TRACKER(TrackerCSRT)
            break;
#else
            UNSUPPORTED_TRACKER("CSRT")
#endif // CV_MAJOR_VERSION
        default:
            std::cout << "Invalid tracker id \"" << type << "\", defaulting to KCF\n";
            trackerName = "KCF";
            tracker = PORTABLE_CREATE_TRACKER(TrackerKCF)
    }
}
