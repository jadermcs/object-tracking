#ifndef INCLUDE_TRACKING_ACCURACY_HPP
#define INCLUDE_TRACKING_ACCURACY_HPP

#include <opencv2/opencv.hpp>

class TrackingAccuracy {
public:

    void addBbox(const cv::Rect2d& bbox, const cv::Rect2d& gt);
    double getRobustness() { return m_robustness; }
    double getJaccardAverage() { return m_jaccardAverage; }
    double getLastJaccard() { return m_jaccardLastCalculated; }

private:

    int32_t m_samples = 0;
    int32_t m_failures = 0;
    double m_robustness = 0.;
    double m_jaccardSum = 0.;
    double m_jaccardAverage = 0.;
    double m_jaccardLastCalculated = 0.;
};

#endif
