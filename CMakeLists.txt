cmake_minimum_required(VERSION 3.1)
project(stereo-vision)

set(CMAKE_CXX_STANDARD 11)
add_definitions(-DCONTRIB_VERSION=4)
find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})
file(GLOB SOURCES src/*.cpp)

# make executable
add_executable(main ${SOURCES})
target_link_libraries(main ${OpenCV_LIBS})
target_include_directories(main PRIVATE src)
