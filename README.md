# Object Tracking

# Compiling

mkdir build
cd build
cmake ..
make

If you have trouble with the static methods ```cv::Tracker::create()``` or ```cv::Tracker::createTracker()```, it means that
the compiling environment has a mismatching version of the OpenCV library and the contrib extra modules.

One of those is version 3 and the other is 4 (e.g.: OpenCV 3 installed with opencv_contrib_4).

The code uses macros to detect which OpenCV version is installed and assumes that the extra modules (contrib) match that version.
If for some reason that isn't the case, it's possible to overwrite the contrib version detection by defining the macro ```CONTRIB_VERSION``` manually. Set it to either ```3``` or ```4```.

If your contrib version uses the static method ```cv::Tracker::create```, then its version is ```4```.

If it uses ```cv::Tracker::createTracker```, the its version is ```3```.

Define the macro ```CONTRIB_VERSION``` accordingly. This can be done by adding the following line to the CMakeLists.txt file:

```add_definitions( -DCONTRIB_VERSION=3 )``` (replace 3 with 4 if you must).

If you are compiling with a compiler directly, you can add the argument ```-DCONTRIB_VERSION=3``` to the compile command to accomplish this.

# Usage

```
./program [params] folder filegt

  -?, -h, --help, --usage (value:true)
          shows this message
  --tracker (value:2)
          algorithm to use for object tracking
           0:BOOSTING
           1:MIL
           2:KCF
           3:TLD
           4:MEDIANFLOW
           5:GOTURN
           6:MOSSE
           7:CSRT

  folder (value:data/car1/)
          frames for tracking folder
  filegt (value:data/gtcar1.txt)
          ground true for bouding boxes
```

To add a param to the program execution, follow the pattern below:

```./program --tracker=4 --folder=data/car2/```
